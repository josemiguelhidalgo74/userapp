package com.user.configuration;

import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class UserConfiguration {

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public GroupedOpenApi i1(Environment environment) {
		if (environment.getProperty("show.internal.api", "true").equalsIgnoreCase("true")) {
			return GroupedOpenApi.builder().group("i1").packagesToScan("com.user.controller.i1").build();
		} else {
			return null;
		}
	}

	@Bean
	public GroupedOpenApi v1() {
		return GroupedOpenApi.builder().group("v1").packagesToScan("com.user.controller.v1").build();
	}

}
