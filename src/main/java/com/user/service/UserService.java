package com.user.service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.webjars.NotFoundException;

import com.user.dto.FinalUserResponse;
import com.user.dto.GenericResponse;
import com.user.dto.ListUserResponse;
import com.user.dto.UserBody;
import com.user.dto.UserResponse;
import com.user.persistence.entity.AppPhone;
import com.user.persistence.entity.AppUser;
import com.user.persistence.repository.AppPhoneRepository;
import com.user.persistence.repository.AppUserRepository;
import com.user.util.Constants;
import com.user.util.JwtUtil;
import com.user.util.Messages;

@Service
public class UserService {

	@Autowired
	private AppUserRepository userRepository;

	@Autowired
	private AppPhoneRepository phoneRepository;

	@Autowired
	private Environment environment;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	private Pattern emailPattern = Pattern.compile(Constants.EMAIL_REGEX);

	public ResponseEntity<GenericResponse> create(@RequestBody UserBody user) {
		return upsert(user, null, Messages.DESCRIPTION_USER_CREATE);
	}

	public ResponseEntity<GenericResponse> update(@RequestBody UserBody user, String id) {
		AppUser appUser = getUser(id);
		return upsert(user, appUser.getId(), Messages.TEXT_SUCCESSFUL_OPERATION);
	}

	public ResponseEntity<GenericResponse> view(String id) {

		return new ResponseEntity<>(
				new FinalUserResponse(Messages.TEXT_SUCCESSFUL_OPERATION, new UserResponse(getUser(id))),
				HttpStatus.OK);
	}

	public ResponseEntity<GenericResponse> list() {
		List<UserResponse> items = new ArrayList<>();
		userRepository.findAll().forEach(appUser -> items.add(new UserResponse(appUser)));

		return new ResponseEntity<>(new ListUserResponse(Messages.TEXT_SUCCESSFUL_OPERATION, items), HttpStatus.OK);
	}

	public ResponseEntity<GenericResponse> delete(String id) {
		userRepository.delete(getUser(id));
		return new ResponseEntity<>(new GenericResponse(Messages.TEXT_SUCCESSFUL_OPERATION), HttpStatus.OK);
	}

	public ResponseEntity<GenericResponse> active(String id, boolean enable) {
		AppUser appUser = getUser(id);
		appUser.setActive(enable);
		userRepository.save(appUser);
		return new ResponseEntity<>(new GenericResponse(Messages.TEXT_SUCCESSFUL_OPERATION), HttpStatus.OK);
	}

	public AppUser getUser(String id) {
		UUID userId;
		try {
			userId = UUID.fromString(id);
		} catch (Exception e) {
			throw new ValidationException(Messages.ERROR_USER_INVALID_ID);
		}
		Optional<AppUser> oAppUser = userRepository.findById(userId);
		if (oAppUser.isEmpty())
			throw new NotFoundException(Messages.ERROR_USER_NOT_FOUND);

		return oAppUser.get();
	}

	public ResponseEntity<GenericResponse> upsert(@RequestBody UserBody user, UUID userId, String message) {
		String passwordRegex = environment.getProperty("user.password.regex");
		if (passwordRegex != null && !passwordRegex.isEmpty()) {
			Pattern pattern = Pattern.compile(passwordRegex);
			if (!pattern.matcher(user.getPassword()).find()) {
				throw new ValidationException(Messages.ERROR_USER_PASSWORD_FORMAT);
			}
		}

		if (!emailPattern.matcher(user.getEmail()).find()) {
			throw new ValidationException(Messages.ERROR_USER_EMAIL_FORMAT);
		}

		user.setPassword(passwordEncoder.encode(user.getPassword()));
		AppUser appUser = new AppUser(user);
		appUser.setId(userId);
		Map<String, Object> claims = new HashMap<>();
		claims.put("creationDate", Instant.now().toEpochMilli());
		appUser.setToken(JwtUtil.generateToken(claims, appUser.getEmail(), environment));
		HttpStatus status = HttpStatus.OK;
		if (userId == null) {
			appUser.setCreated(Instant.now());
			appUser.setLastLogin(Instant.now());
			status = HttpStatus.CREATED;
		} else {
			phoneRepository.deleteAllByAppUserReference(appUser);
		}

		List<AppPhone> phones = new ArrayList<>();
		user.getPhones().forEach(phone -> {
			AppPhone newPhone = new AppPhone(phone);
			newPhone.setAppUserReference(appUser);
			phones.add(newPhone);
		});
		appUser.setPhones(phones);
		AppUser result = userRepository.save(appUser);
		phoneRepository.saveAll(phones);

		return new ResponseEntity<>(new FinalUserResponse(message, new UserResponse(result)), status);
	}

}
