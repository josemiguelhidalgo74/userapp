package com.user.service;

import java.time.Instant;
import java.util.regex.Pattern;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.user.dto.GenericResponse;
import com.user.dto.LoginBody;
import com.user.dto.TokenResponse;
import com.user.exceptions.ForbiddenException;
import com.user.exceptions.UnauthorizedException;
import com.user.persistence.entity.AppUser;
import com.user.persistence.repository.AppUserRepository;
import com.user.util.JwtUtil;
import com.user.util.Messages;

@Service
public class MyselfService {
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	@Autowired
	private AppUserRepository userRepository;
	@Autowired
	private Environment environment;

	public ResponseEntity<GenericResponse> login(LoginBody login) {
		AppUser user = userRepository.findByEmail(login.getEmail());
		if (user == null || !user.isActive())
			throw new ForbiddenException(Messages.ERROR_USER_FORBIDDEN);

		if (!passwordEncoder.matches(login.getPassword(), user.getPassword()))
			throw new UnauthorizedException(Messages.ERROR_USER_UNAUTHORIZED);
		user.setLastLogin(Instant.now());
		userRepository.save(user);
		return new ResponseEntity<>(new TokenResponse(Messages.TEXT_SUCCESSFUL_OPERATION, user.getToken()),
				HttpStatus.OK);

	}

	public ResponseEntity<GenericResponse> updatePasword(String token, String password) {
		String jwtUser = JwtUtil.getUser(token, environment.getProperty("jwt.secret"));
		if (jwtUser == null)
			throw new ForbiddenException(Messages.ERROR_USER_FORBIDDEN);
		AppUser user = userRepository.findByEmail(jwtUser);
		if (user == null)
			throw new ForbiddenException(Messages.ERROR_USER_FORBIDDEN);
		String passwordRegex = environment.getProperty("user.password.regex");
		if (passwordRegex != null && !passwordRegex.isEmpty()) {
			Pattern pattern = Pattern.compile(passwordRegex);
			if (!pattern.matcher(password).find()) {
				throw new ValidationException(Messages.ERROR_USER_PASSWORD_FORMAT);
			}
		}
		user.setPassword(passwordEncoder.encode(password));
		user.setModified(Instant.now());
		userRepository.save(user);
		return new ResponseEntity<>(new GenericResponse(Messages.TEXT_SUCCESSFUL_OPERATION), HttpStatus.OK);

	}

}
