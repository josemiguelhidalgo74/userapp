package com.user.controller.i1;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.user.dto.FinalUserResponse;
import com.user.dto.GenericResponse;
import com.user.dto.ListUserResponse;
import com.user.dto.UserBody;
import com.user.exceptions.ProcessExctions;
import com.user.service.UserService;
import com.user.util.Messages;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping(path = "/i1/users")
@SecurityRequirement(name = "Jwt_Token")
@Tag(name = "Users", description = "Mechanisms to manage users")
public class UserController {

	@Autowired
	private UserService userService;

	@Operation(summary = Messages.TITLE_USER_CREATE)
	@ApiResponses(value = {
			@ApiResponse(responseCode = "201", description = Messages.DESCRIPTION_USER_CREATE, content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = FinalUserResponse.class)) }),
			@ApiResponse(responseCode = "400", description = "Bad Request", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ResponseEntity.class)) }),
			@ApiResponse(responseCode = "409", description = "Conflict", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ResponseEntity.class)) }),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ResponseEntity.class)) }) })
	@PostMapping()
	public ResponseEntity<GenericResponse> create(@Valid @RequestBody UserBody user) {
		try {
			return userService.create(user);
		} catch (Exception e) {
			return ProcessExctions.generatErrorResponse(e);
		}

	}

	@Operation(summary = Messages.TITLE_USER_UPDATE)
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = Messages.TEXT_SUCCESSFUL_OPERATION, content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = FinalUserResponse.class)) }),
			@ApiResponse(responseCode = "400", description = "Bad Request", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ResponseEntity.class)) }),
			@ApiResponse(responseCode = "404", description = "Not Found", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ResponseEntity.class)) }),
			@ApiResponse(responseCode = "409", description = "Conflict", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ResponseEntity.class)) }),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ResponseEntity.class)) }) })
	@PutMapping("/{id}")
	public ResponseEntity<GenericResponse> update(@PathVariable(name = "id") String userId,
			@Valid @RequestBody UserBody user) {
		try {
			return userService.update(user, userId);
		} catch (Exception e) {
			return ProcessExctions.generatErrorResponse(e);
		}

	}

	@Operation(summary = Messages.TITLE_USER_VIEW)
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = Messages.TEXT_SUCCESSFUL_OPERATION, content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = FinalUserResponse.class)) }),
			@ApiResponse(responseCode = "400", description = "Bad Request", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ResponseEntity.class)) }),
			@ApiResponse(responseCode = "404", description = "Not Found", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ResponseEntity.class)) }),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ResponseEntity.class)) }) })
	@GetMapping("/{id}")
	public ResponseEntity<GenericResponse> view(@PathVariable(name = "id") String userId) {
		try {
			return userService.view(userId);
		} catch (Exception e) {
			return ProcessExctions.generatErrorResponse(e);
		}
	}

	@Operation(summary = Messages.TITLE_USER_LISTE)
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = Messages.TEXT_SUCCESSFUL_OPERATION, content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ListUserResponse.class)) }),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ResponseEntity.class)) }) })
	@GetMapping
	public ResponseEntity<GenericResponse> list() {
		try {
			return userService.list();
		} catch (Exception e) {
			return ProcessExctions.generatErrorResponse(e);
		}
	}

	@Operation(summary = Messages.TITLE_USER_DELETE)
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = Messages.TEXT_SUCCESSFUL_OPERATION, content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = GenericResponse.class)) }),
			@ApiResponse(responseCode = "404", description = "Not Found", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ResponseEntity.class)) }),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ResponseEntity.class)) }) })
	@DeleteMapping("/{id}")
	public ResponseEntity<GenericResponse> delete(@PathVariable(name = "id") String userId) {
		try {
			return userService.delete(userId);
		} catch (Exception e) {
			return ProcessExctions.generatErrorResponse(e);
		}
	}

	@Operation(summary = Messages.TITLE_USER_ACTIVE)
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = Messages.TEXT_SUCCESSFUL_OPERATION, content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = GenericResponse.class)) }),
			@ApiResponse(responseCode = "404", description = "Not Found", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ResponseEntity.class)) }),
			@ApiResponse(responseCode = "500", description = "Internal Server Error", content = {
					@Content(mediaType = "application/json", schema = @Schema(implementation = ResponseEntity.class)) }) })
	@PatchMapping("/{id}/active")
	public ResponseEntity<GenericResponse> active(@PathVariable(name = "id") String userId,
			@RequestParam(name = "enable", required = true) Boolean enable) {
		try {
			return userService.active(userId, enable);
		} catch (Exception e) {
			return ProcessExctions.generatErrorResponse(e);
		}
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public GenericResponse handleValidationExceptions(MethodArgumentNotValidException ex) {
		String message = null;
		try {
			Optional<ObjectError> value = ex.getBindingResult().getAllErrors().stream().findFirst();
			if (value.isPresent())
				message = value.get().getDefaultMessage();
		} catch (Exception e) {
			message = Messages.ERROR_UNDETERMINED_BAD_REQUEST;
		}
		return new GenericResponse(message);
	}

}
