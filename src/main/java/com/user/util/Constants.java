package com.user.util;

public class Constants {

	private Constants() {
		throw new IllegalStateException("Constants class");
	}

	public static final String HEADER_AUTHORIZATION = "Authorization";
	public static final String TOKEN_PREFIX = "Bearer";
	public static final String EMAIL_REGEX ="[_A-Za-z0-9-]+((\\.|\\+)?[_A-Za-z0-9-]+)*@[A-Za-z0-9]+((\\.|-)?[A-Za-z0-9]+)*(\\.[A-Za-z]{2,6})$";

}
