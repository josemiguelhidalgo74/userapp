package com.user.controller;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.user.controller.v1.MyselfController;
import com.user.dto.GenericResponse;
import com.user.dto.LoginBody;
import com.user.dto.PasswordBody;
import com.user.dto.TokenResponse;
import com.user.service.MyselfService;
import com.user.util.Constants;
import com.user.util.Messages;

@WebMvcTest(MyselfController.class)
class MyselfControllerTest {

	@MockBean
	private MyselfService myselfService;

	@Autowired
	private MockMvc mockMvc;

	private ObjectMapper omMapper = new ObjectMapper();

	private String basicPath = "/v1/myself";

	@BeforeEach
	void setup() throws Exception {
		Mockito.when(myselfService.login(Mockito.any(LoginBody.class))).thenReturn(new ResponseEntity<>(
				new TokenResponse(Messages.TEXT_SUCCESSFUL_OPERATION, "xxxx-yyyyy-zzzz"), HttpStatus.OK));

		Mockito.when(myselfService.updatePasword(Mockito.anyString(), Mockito.anyString())).thenReturn(
				new ResponseEntity<>(new GenericResponse(Messages.TEXT_SUCCESSFUL_OPERATION), HttpStatus.OK));
	}

	@Test
	void loginTest() throws Exception {
		mockMvc.perform(post(String.format("%s/login", basicPath)).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(omMapper.writeValueAsString(new LoginBody("juan@rodriguez.org", "hunter2*"))))
				.andExpect(status().is(HttpStatus.OK.value())).andExpect(jsonPath("$.message").exists())
				.andExpect(jsonPath("$.message", is(Messages.TEXT_SUCCESSFUL_OPERATION)))
				.andExpect(jsonPath("$.token").exists()).andExpect(jsonPath("$.token", is("xxxx-yyyyy-zzzz")));
	}

	@Test
	void updatePaswordTest() throws Exception {
		mockMvc.perform(patch(String.format("%s/password", basicPath)).contentType(MediaType.APPLICATION_JSON).header(
				Constants.HEADER_AUTHORIZATION,
				"Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJqdWFuQHJvZHJpZ3Vlei5vcmciLCJjcmVhdGlvbkRhdGUiOjE2NDk1MzU1MzYyMjMsImlhdCI6MTY0OTUzNTUzNn0.YY_uMAVZN6JW0WRNfOaf67sXhCmfE1SOjB098cdvS7Pb7v0mRbP5ZXQ9_t0SCcAaSiuizlRXYn1CdJfbMwMl5w")
				.accept(MediaType.APPLICATION_JSON).content(omMapper.writeValueAsString(new PasswordBody("hunter2*"))))
				.andExpect(status().is(HttpStatus.OK.value())).andExpect(jsonPath("$.message").exists())
				.andExpect(jsonPath("$.message", is(Messages.TEXT_SUCCESSFUL_OPERATION)));
	}

}
