package com.user.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import javax.validation.ValidationException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.user.dto.GenericResponse;
import com.user.dto.LoginBody;
import com.user.exceptions.ForbiddenException;
import com.user.exceptions.UnauthorizedException;
import com.user.persistence.entity.AppUser;
import com.user.persistence.repository.AppUserRepository;
import com.user.service.MyselfService;
import com.user.util.JwtUtil;
import com.user.util.Messages;
import com.user.util.TestUtils;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class MyselfServiceTest {

	@InjectMocks
	MyselfService myselfService;

	@Mock
	private Environment environment;

	@Autowired
	private Environment env;

	@Mock
	BCryptPasswordEncoder passwordEncoder;

	@Mock
	AppUserRepository userRepository;

	private String uId = "1a000000-b000-c000-d000-e00000000000";

	private AppUser newAppUser;

	@BeforeEach
	public void setup() {
		MockitoAnnotations.openMocks(this);
		String reference = "test";
		newAppUser = TestUtils.createUser(reference, uId);
	}

	@Test
	void loginTest() {
		when(userRepository.findByEmail(Mockito.anyString())).thenReturn(newAppUser);
		when(passwordEncoder.matches(Mockito.anyString(), Mockito.anyString())).thenReturn(true);
		ResponseEntity<GenericResponse> result = myselfService.login(new LoginBody(newAppUser.getEmail(), "hunter2*"));
		assertNotNull("The result cannot be null", result);
		assertEquals(HttpStatus.OK, result.getStatusCode());
		assertEquals(Messages.TEXT_SUCCESSFUL_OPERATION, result.getBody().getMessage());
	}

	@Test
	void loginEmailDisableTest() {
		String reference = "err";
		AppUser newAppUser = TestUtils.createUser(reference, uId);
		newAppUser.setActive(false);
		when(userRepository.findByEmail(Mockito.anyString())).thenReturn(newAppUser);
		ResponseEntity<GenericResponse> result = null;
		try {
			result = myselfService.login(new LoginBody("juan@rodriguez.org", "hunter2*"));
		} catch (Exception e) {
			assertTrue(e instanceof ForbiddenException);
		}
		assertNull(result);
	}

	@Test
	void loginEmailNullTest() {
		when(userRepository.findByEmail(Mockito.anyString())).thenReturn(null);
		ResponseEntity<GenericResponse> result = null;
		try {
			result = myselfService.login(new LoginBody("juan@rodriguez.org", "hunter2*"));
		} catch (Exception e) {
			assertTrue(e instanceof ForbiddenException);
		}
		assertNull(result);
	}

	@Test
	void loginPasswordErrorMatcheTest() {
		when(userRepository.findByEmail(Mockito.anyString())).thenReturn(newAppUser);
		when(passwordEncoder.matches(Mockito.anyString(), Mockito.anyString())).thenReturn(false);
		ResponseEntity<GenericResponse> result = null;
		try {
			result = myselfService.login(new LoginBody("juan@rodriguez.org", "hunter2*"));
		} catch (Exception e) {
			assertTrue(e instanceof UnauthorizedException);
		}
		assertNull(result);
	}

	@Test
	void updatePaswordTest() {
		try (MockedStatic<JwtUtil> mockedStatic = Mockito.mockStatic(JwtUtil.class)) {
			when(JwtUtil.getUser(Mockito.anyString(), Mockito.anyString())).thenReturn("juan@rodriguez.org");
			when(environment.getProperty("jwt.secret")).thenReturn(env.getProperty("jwt.secret"));
			when(userRepository.findByEmail(Mockito.anyString())).thenReturn(newAppUser);
			ResponseEntity<GenericResponse> result = myselfService.updatePasword(newAppUser.getEmail(), "hunter2*");
			assertNotNull("The result cannot be null", result);
			assertEquals(HttpStatus.OK, result.getStatusCode());
			assertEquals(Messages.TEXT_SUCCESSFUL_OPERATION, result.getBody().getMessage());
		}
	}

	@Test
	void updatePaswordInvalidTokenTest() {
		try (MockedStatic<JwtUtil> mockedStatic = Mockito.mockStatic(JwtUtil.class)) {
			when(JwtUtil.getUser(Mockito.anyString(), Mockito.anyString())).thenReturn(null);
			ResponseEntity<GenericResponse> result = null;
			try {
				result = myselfService.updatePasword("xxxx", "hunter2*");
			} catch (Exception e) {
				assertTrue(e instanceof ForbiddenException);
			}
			assertNull(result);
		}
	}

	@Test
	void updatePaswordEmailNullTest() {
		try (MockedStatic<JwtUtil> mockedStatic = Mockito.mockStatic(JwtUtil.class)) {
			when(JwtUtil.getUser(Mockito.anyString(), Mockito.anyString())).thenReturn("juan@rodriguez.org");
			when(environment.getProperty("jwt.secret")).thenReturn(env.getProperty("jwt.secret"));
			when(userRepository.findByEmail(Mockito.anyString())).thenReturn(null);
			ResponseEntity<GenericResponse> result = null;
			try {
				result = myselfService.updatePasword("xxxx", "hunter2*");
			} catch (Exception e) {
				assertTrue(e instanceof ForbiddenException);
			}
			assertNull(result);
		}
	}

	@Test
	void updatePaswordErrorFormatTest() {
		try (MockedStatic<JwtUtil> mockedStatic = Mockito.mockStatic(JwtUtil.class)) {
			when(JwtUtil.getUser(Mockito.anyString(), Mockito.anyString())).thenReturn(newAppUser.getEmail());
			when(environment.getProperty("jwt.secret")).thenReturn(env.getProperty("jwt.secret"));
			when(userRepository.findByEmail(Mockito.anyString())).thenReturn(newAppUser);
			when(environment.getProperty("user.password.regex")).thenReturn("yyyy");
			ResponseEntity<GenericResponse> result = null;
			try {
				result = myselfService.updatePasword("xxxx", "hunter2*");
			} catch (Exception e) {
				assertTrue(e instanceof ValidationException);
			}
			assertNull(result);
		}
	}
}
