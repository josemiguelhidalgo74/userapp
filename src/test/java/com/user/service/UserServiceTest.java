package com.user.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import javax.validation.ValidationException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.webjars.NotFoundException;

import com.user.dto.GenericResponse;
import com.user.dto.UserBody;
import com.user.persistence.entity.AppUser;
import com.user.persistence.repository.AppPhoneRepository;
import com.user.persistence.repository.AppUserRepository;
import com.user.util.Messages;
import com.user.util.TestUtils;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
class UserServiceTest {

	@InjectMocks
	private UserService userService;

	@Mock
	private Environment environment;

	@Autowired
	private Environment env;

	@Mock
	BCryptPasswordEncoder passwordEncoder;

	@Mock
	AppUserRepository userRepository;

	@Mock
	AppPhoneRepository phoneRepository;

	@BeforeEach
	public void setup() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void createTest() {
		UUID id = UUID.randomUUID();
		AppUser newAppUser = TestUtils.createUser("new", id.toString());
		UserBody uBody = new UserBody(newAppUser);
		uBody.setPassword(newAppUser.getPassword());
		when(environment.getProperty("user.password.regex")).thenReturn(env.getProperty("user.password.regex"));
		when(passwordEncoder.encode(Mockito.anyString())).thenReturn("xxxxxx");
		when(environment.getProperty("jwt.secret")).thenReturn(env.getProperty("jwt.secret"));
		when(userRepository.save(Mockito.any(AppUser.class))).thenReturn(newAppUser);
		ResponseEntity<GenericResponse> result = userService.create(uBody);
		assertNotNull("The result cannot be null", result);
		assertEquals(HttpStatus.CREATED, result.getStatusCode());
		assertEquals(Messages.DESCRIPTION_USER_CREATE, result.getBody().getMessage());
	}

	@Test
	void updateTest() {
		UUID id = UUID.randomUUID();
		AppUser newAppUser = TestUtils.createUser("new", id.toString());
		UserBody uBody = new UserBody(newAppUser);
		uBody.setPassword(newAppUser.getPassword());
		when(environment.getProperty("user.password.regex")).thenReturn(env.getProperty("user.password.regex"));
		when(userRepository.findById(id)).thenReturn(Optional.of(newAppUser));
		when(passwordEncoder.encode(Mockito.anyString())).thenReturn("xxxxxx");
		when(environment.getProperty("jwt.secret")).thenReturn(env.getProperty("jwt.secret"));
		when(userRepository.save(Mockito.any(AppUser.class))).thenReturn(newAppUser);
		ResponseEntity<GenericResponse> result = userService.update(uBody, newAppUser.getId().toString());
		assertNotNull("The result cannot be null", result);
		assertEquals(HttpStatus.OK, result.getStatusCode());
		assertEquals(Messages.TEXT_SUCCESSFUL_OPERATION, result.getBody().getMessage());
	}

	@Test
	void viewTest() {
		UUID id = UUID.randomUUID();
		AppUser newAppUser = TestUtils.createUser("new", id.toString());
		when(userRepository.findById(id)).thenReturn(Optional.of(newAppUser));
		ResponseEntity<GenericResponse> result = userService.view(id.toString());
		assertNotNull("The result cannot be null", result);
		assertEquals(HttpStatus.OK, result.getStatusCode());
		assertEquals(Messages.TEXT_SUCCESSFUL_OPERATION, result.getBody().getMessage());
	}

	@Test
	void listTest() {
		UUID id = UUID.randomUUID();
		AppUser newAppUser = TestUtils.createUser("new", id.toString());
		when(userRepository.findAll()).thenReturn(Arrays.asList(newAppUser));
		ResponseEntity<GenericResponse> result = userService.list();
		assertNotNull("The result cannot be null", result);
		assertEquals(HttpStatus.OK, result.getStatusCode());
		assertEquals(Messages.TEXT_SUCCESSFUL_OPERATION, result.getBody().getMessage());
	}

	@Test
	void deleteTest() {
		UUID id = UUID.randomUUID();
		AppUser newAppUser = TestUtils.createUser("new", id.toString());
		when(userRepository.findById(id)).thenReturn(Optional.of(newAppUser));
		ResponseEntity<GenericResponse> result = userService.delete(id.toString());
		assertNotNull("The result cannot be null", result);
		assertEquals(HttpStatus.OK, result.getStatusCode());
		assertEquals(Messages.TEXT_SUCCESSFUL_OPERATION, result.getBody().getMessage());
	}

	@Test
	void activeTest() {
		UUID id = UUID.randomUUID();
		AppUser newAppUser = TestUtils.createUser("new", id.toString());
		when(userRepository.findById(id)).thenReturn(Optional.of(newAppUser));
		ResponseEntity<GenericResponse> result = userService.active(id.toString(), true);
		assertNotNull("The result cannot be null", result);
		assertEquals(HttpStatus.OK, result.getStatusCode());
		assertEquals(Messages.TEXT_SUCCESSFUL_OPERATION, result.getBody().getMessage());
	}

	@Test
	void idInvalidFormatTest() {
		ResponseEntity<GenericResponse> result = null;
		try {
			result = userService.view("xxxx");
		} catch (Exception e) {
			assertTrue(e instanceof ValidationException);
		}
		assertNull(result);
	}

	@Test
	void userNotFoundTest() {
		ResponseEntity<GenericResponse> result = null;
		try {
			UUID id = UUID.randomUUID();
			result = userService.view(id.toString());
		} catch (Exception e) {
			assertTrue(e instanceof NotFoundException);
		}
		assertNull(result);
	}

	@Test
	void paswordErrorFormatTest() {

		UUID id = UUID.randomUUID();
		AppUser newAppUser = TestUtils.createUser("new", id.toString());
		UserBody uBody = new UserBody(newAppUser);
		uBody.setPassword(newAppUser.getPassword());
		when(environment.getProperty("user.password.regex")).thenReturn("yyyyy");
		ResponseEntity<GenericResponse> result = null;
		try {
			result = userService.create(uBody);
		} catch (Exception e) {
			assertTrue(e instanceof ValidationException);
		}
		assertNull(result);
	}

	@Test
	void emailErrorFormatTest() {
		UUID id = UUID.randomUUID();
		AppUser newAppUser = TestUtils.createUser("new", id.toString());
		UserBody uBody = new UserBody(newAppUser);
		uBody.setPassword(newAppUser.getPassword());
		uBody.setEmail("xxxxxxxx");
		when(environment.getProperty("user.password.regex")).thenReturn(env.getProperty("user.password.regex"));
		ResponseEntity<GenericResponse> result = null;
		try {
			result = userService.create(uBody);
		} catch (Exception e) {
			assertTrue(e instanceof ValidationException);
		}
		assertNull(result);
	}

}
